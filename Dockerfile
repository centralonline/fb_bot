FROM node:10.4.1-alpine

WORKDIR /app

COPY . /app
RUN chmod 755 /app
RUN npm install

EXPOSE 3000
CMD [ "npm", "start" ]
