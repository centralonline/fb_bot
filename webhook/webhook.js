const
    request = require('request')
    , config = require('config')
    , PAGE_ACCESS_TOKEN = config.get('pageAccessToken')

module.exports = {
    verification: function (validationToken,mode,token) {

        // Checks if a token and mode is in the query string of the request
        if (mode && token) {
            let returnStatusMsg = 403;

            // Checks the mode and token sent is correct
            if (mode === 'subscribe' && token === validationToken) {

                // Responds with the challenge token from the request
                console.log('WEBHOOK_VERIFIED');
                returnStatusMsg = 200

            } else {
                // Responds with '403 Forbidden' if verify tokens do not match
                console.log("validationToken => ", validationToken);
                returnStatusMsg = 403
            }
            return returnStatusMsg;
        }
    },
    // generic function sending messages
    sendMessage: function (recipientId, message) {
        request({
            url: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {access_token: PAGE_ACCESS_TOKEN},
            method: 'POST',
            json: {
                recipient: {id: recipientId},
                message: message
            }
        }, function (error, response, body) {
            if (error) {
                console.log('Error sending message: ', error);
            } else if (response.body.error) {
                console.log('Error: sendMessage() => response.body.error => ', response.body.error);
            }
        });
    },


    handleMessage: function (sender_psid, received_message) {
        console.log("handleMessage()");
        let response;

        // Checks if the message contains text
        if (received_message.text) {

            // Creates the payload for a basic text message, which
            // will be added to the body of our request to the Send API
            response = {
                "text": `You sent the message: "${received_message.text}". Now send me an attachment!`
            }

        } else if (received_message.attachments) {
            // Get the URL of the message attachment
            let attachment_url = received_message.attachments[0].payload.url;
            response = {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",
                        "elements": [{
                            "title": "Is this the right picture?",
                            "subtitle": "Tap a button to answer.",
                            "image_url": attachment_url,
                            "buttons": [
                                {
                                    "type": "postback",
                                    "title": "Yes!",
                                    "payload": "yes",
                                },
                                {
                                    "type": "postback",
                                    "title": "No!",
                                    "payload": "no",
                                }
                            ],
                        }]
                    }
                }
            }
        }

        // Send the response message
        this.callSendAPI(sender_psid, response);
    },

    handlePostback: function (sender_psid, received_postback) {
        console.log("handlePostback()");
        let response;

        // Get the payload for the postback
        let payload = received_postback.payload;

        // Set the response based on the postback payload
        if (payload === 'yes') {
            response = {"text": "Thanks!"}
        } else if (payload === 'no') {
            response = {"text": "Oops, try sending another image."}
        }
        // Send the message to acknowledge the postback
        this.callSendAPI(sender_psid, response);
    },

    callSendAPI: function (sender_psid, response) {
        console.log("callSendAPI()");
        // Construct the message body
        let request_body = {
            "recipient": {
                "id": sender_psid
            },
            "message": response
        };

        // Send the HTTP request to the Messenger Platform
        request({
            "uri": "https://graph.facebook.com/v2.6/me/messages",
            "qs": {"access_token": PAGE_ACCESS_TOKEN},
            "method": "POST",
            "json": request_body
        }, (err, res, body) => {
            if (!err) {
                console.log('message sent!')
            } else {
                console.error("Unable to send message:" + err);
            }
        });
    }
};