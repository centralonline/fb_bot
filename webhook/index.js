const
    express = require('express')
    , router = express.Router()
    , config = require('config')
    , validationToken = config.get('validationToken')
    , webhook = require("./webhook")
    , messageQ = JSON.stringify(require('./message.json'))

// Verification
router.get('/', (req, res) => {
    let mode = req.query['hub.mode'];
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];
    let verifyRes = webhook.verification(validationToken, mode, token, challenge);

    if (verifyRes === 200) {
        res.status(200).send(challenge);
    } else {
        res.sendStatus(403);
    }
});

// handler receiving messages
router.post('/', function (req, res) {
    console.log(" \n\n\n\n\n ************************************************ ");
    console.log(" ******************** " + new Date().toISOString() + " ****************** ");
    console.log(" ************************************************ ");
    let body = req.body;
    if (body.object === 'page') {
        // Iterate over each entry - there may be multiple if batched
        body.entry.forEach(function (entry) {

                // Get the webhook event. entry.messaging is an array, but
                // will only ever contain one event, so we get index 0
                let webhook_event = entry.messaging[0];
                console.log("DEBUG: webhook_event => ", webhook_event);

                // Get the sender PSID
                let sender_psid = webhook_event.sender.id;
                console.log('DEBUG: Sender PSID => ' + sender_psid);

                // *******************
                // Check if the event is a message or postback and
                // pass the event to the appropriate handler function
                // if (webhook_event.message) {
                //     webhook.handleMessage(sender_psid, webhook_event.message);
                // } else if (webhook_event.postback) {
                //     webhook.handlePostback(sender_psid, webhook_event.postback);
                // }
                // #*******************

                let userchoose = [];
                console.log("DEBUG: userchoose => ", userchoose);
                // set time to remove choose
                if (userchoose.find(user => user.userid === sender_psid) !== undefined) {
                    console.log("DEBUG: set time to remove choose");
                    if (userchoose.find(user => user.userid === sender_psid).timestamp < new Date().getTime() - (1000 * 60 * 5)) {
                        userchoose.removeItem("userid", sender_psid);
                    }
                }

                if (webhook_event.message && (userchoose.find(user => user.userid === sender_psid) === undefined)) {
                    console.log('DEBUG: webhook_event.message => ' + JSON.stringify(webhook_event.message));
                    webhook.sendMessage(sender_psid, JSON.parse(messageQ).choose_department);
                } else if (webhook_event.postback) {
                    console.log("DEBUG: Postback received webhook_event.postback => " + JSON.stringify(webhook_event.postback));
                    userchoose.push({
                        userid: sender_psid,
                        payload: webhook_event.postback.payload,
                        timestamp: webhook_event.timestamp
                    });
                    console.log("DEBUG: userchoose => " + JSON.stringify(userchoose));
                    console.log("DEBUG: webhook_event.postback.payload => " + webhook_event.postback.payload);
                    webhook.sendMessage(sender_psid, {"text": "channel: " + webhook_event.postback.payload});
                } else if (userchoose.find(user => user.userid === sender_psid) !== undefined) {
                    // if (typeof userchoose.find(user => user.userid === sender_psid).payload !== undefined) {
                        if (userchoose.find(user => user.userid === sender_psid).payload === 'online') {
                            console.log('DEBUG: sent callback to online');
                        } else if (userchoose.find(user => user.userid === sender_psid).payload === 'store') {
                            console.log('DEBUG: sent callback to store');
                        }
                    // }

                }
            }
        );
        console.log(" ================================================ ");
        console.log(" ================================================ ");
        console.log(" ================================================\n\n\n\n\n ");

        // Return a '200 OK' response to all events
        res.status(200).send('EVENT_RECEIVED');
    } else {
        // Return a '404 Not Found' if event is not from a page subscription
        res.sendStatus(404);
    }
})
;

Object.prototype.removeItem = function (key, value) {
    if (value == undefined)
        return;

    for (var i in this) {
        if (this[i][key] == value) {
            this.splice(i, 1);
        }
    }
};


module.exports = router;