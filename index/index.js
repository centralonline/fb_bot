const
    express = require('express'),
    router = express.Router();

router.get('/', (_, res) => {
    res.status(200).json({ message: 'This is TestBot Server 5' });
});

module.exports = router;