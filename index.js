'use strict';

const
    express = require('express')
    , bodyParser = require('body-parser')
    , app = express()
    , appPort = process.env.PORT || 3000
    , index = require('./index/index')
    , webhook = require('./webhook/index');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/', index);
app.use('/webhook', webhook);

app.listen(appPort, () => {
    console.log('Node app is running on port ', appPort);
});
