# CDS COD FbChatBot

## RUN

```sh
$ npm install

$ npm start

$ npm run start:local
OR
$ NODE_ENV=localhost node app.js
```

# Environment dev

- https://cds-cod-fbchatbot-dev.central.tech/
- http://cds-cod-fbchatbot-dev-2045042167.ap-southeast-1.elb.amazonaws.com/


## Install aws-cli

https://docs.aws.amazon.com/cli/latest/userguide/installing.html

### SSH config

```sh
chmod 600 {path}/COD_DEV.pem
```

```sh
$ vim ~/.ssh/config

Host cds.cod.fbchatbot.dev
  HostName 10.86.20.71
  User ubuntu
  Port 22
  IdentityFile ~/Downloads/pem/COD_DEV.pem
```


### Map hosts


```sh
nslookup cds-cod-fbchatbot-dev-2045042167.ap-southeast-1.elb.amazonaws.com
```

```sh
$ vim /etc/hosts

#Map CENTRAL.tech
54.169.156.166 cds-cod-fbchatbot-dev.central.tech
```


## Deploy

```sh
$ sh deploy-dev.sh -tag latest

$ sh deploy-dev.sh -pemfile ~/Downloads/pem/COD_DEV.pem -tag latest

```



