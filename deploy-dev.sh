#!/bin/bash

while echo $1 | grep -q ^-; do
    eval $( echo $1 | sed 's/^-//' )=$2
    shift
    shift
done

if [ "$tag" == "" ]; then
	tag="latest"
fi

if [ "$pemfile" == "" ]; then
	pemfile="~/Downloads/pem/COD_DEV.pem"
	echo "$ sh deploy-dev.sh -pemfile ~/Downloads/pem/COD_DEV.pem"
	SSHC="cod.fbchatbot.dev"
else
    SSHC="-i $pemfile ubuntu@10.86.20.71"
fi

echo
echo pem = $pemfile
echo tag = $tag
#echo args = $@

echo
echo "Login AWS"
eval $(aws ecr get-login --no-include-email)
echo
echo "Docker"
docker build -t cod-cds-fbchatbot-dev:$tag .
echo
docker tag cod-cds-fbchatbot-dev:$tag 770081661684.dkr.ecr.ap-southeast-1.amazonaws.com/cod-cds-fbchatbot-dev:$tag
echo
docker push 770081661684.dkr.ecr.ap-southeast-1.amazonaws.com/cod-cds-fbchatbot-dev:$tag
echo

echo
echo "SSH $ docker stop fb_chat_bot"
ssh $SSHC docker stop fb_chat_bot
ssh $SSHC docker rmi 770081661684.dkr.ecr.ap-southeast-1.amazonaws.com/cod-cds-fbchatbot-dev:$tag
echo
echo "SSH $ docker run fb_chat_bot"
ssh $SSHC eval $(aws ecr get-login --no-include-email)
ssh $SSHC docker run -d --rm --name fb_chat_bot -p 80:3000 770081661684.dkr.ecr.ap-southeast-1.amazonaws.com/cod-cds-fbchatbot-dev:$tag





